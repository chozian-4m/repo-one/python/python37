# python37
Python is developed under an OSI-approved open source license, making it freely usable and distributable, even for commercial use. Python's license is administered by the Python Software Foundation.

Connect to [https://www.python.org/](https://www.python.org/)for complete details about Python.

# More resources
[Python 3.7.10 Online Documentation](https://www.python.org/downloads/release/python-3710/)
Report bugs at [https://bugs.python.org](https://bugs.python.org).
