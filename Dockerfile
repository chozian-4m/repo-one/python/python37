ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG} as build

RUN dnf upgrade -y --nodocs && \
    dnf install -y --nodocs \
       bzip2-devel \
       expat-devel \
       gcc \
       libffi-devel \
       libuuid-devel \
       make \
       openssl-devel \
       sqlite-devel \
       xz-devel && \
    dnf clean all && \
    rm -rf /var/cache/dnf

COPY python.tar.gz /

RUN tar -xzvf python.tar.gz && \
    cd cpython-3.7.13 && \
    ./configure \
      --enable-loadable-sqlite-extensions \
      --enable-optimizations \
      --enable-option-checking=fatal \
      --with-system-expat \
      --with-ensurepip && \
    make altinstall

RUN find /usr/local -depth \
	\( \
		\( -type d -a \( -name test -o -name tests -o -name idle_test \) \) \
		-o \( -type f -a \( -name '*.pyc' -o -name '*.pyo' -o -name '*.a' \) \) \
	\) -exec rm -rf '{}' + && \
    echo '/usr/local/lib' > /etc/ld.so.conf && \
    ldconfig

COPY pip-21.3.1-py3-none-any.whl /
COPY setuptools-60.5.0-py3-none-any.whl /
COPY wheel-0.37.1-py2.py3-none-any.whl /

RUN python3.7 -m pip install --no-index --upgrade /pip-21.3.1-py3-none-any.whl && \
    python3.7 -m pip install --no-index --upgrade /setuptools-60.5.0-py3-none-any.whl && \
    python3.7 -m pip install --no-index --upgrade /wheel-0.37.1-py2.py3-none-any.whl

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

RUN dnf update -y --nodocs && \
    dnf clean all && \
    rm -rf /var/cache/dnf

ENV PATH /usr/local/bin:$PATH

COPY --from=build /usr/local/include/python3.7m /usr/local/include/python3.7m
COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/bin /usr/local/bin

RUN cd /usr/local/bin && \
    ln -s idle3.7 idle3 && \
    ln -s idle3 idle && \
    ln -s pydoc3.7 pydoc3 && \
    ln -s pydoc3 pydoc && \
    ln -s python3.7 python3 && \
    ln -s python3 python && \
    ln -s python3.7-config python3-config && \
    ln -s python3-config python-config && \
    ln -s easy_install-3.7 easy_install-3 && \
    ln -s easy_install-3 easy_install && \
    ln -s 2to3-3.7 2to3-3 && \
    ln -s 2to3-3 2to3 && \
    echo '/usr/local/lib' > /etc/ld.so.conf && \
    ldconfig

RUN groupadd -g 1001 python && \
    useradd -r -u 1001 -m -s /sbin/nologin -g python python

USER 1001

CMD ["python3"]

HEALTHCHECK NONE
